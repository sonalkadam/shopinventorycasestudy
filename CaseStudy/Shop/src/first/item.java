package first;

public class item {
	public int item_id;
	public String name;
	public int qty;
	public int rate;
	public item() {
		super();
	}
	public item(int item_id, String name, int qty, int rate) {
		super();
		this.item_id = item_id;
		this.name = name;
		this.qty = qty;
		this.rate = rate;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		return "item [item_id=" + item_id + ", name=" + name + ", qty=" + qty + ", rate=" + rate + "]";
	}
	

}
